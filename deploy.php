<?php

namespace Deployer;

require 'recipe/wordpress.php';

// Prepare constants
define('_SSH_HOST_', getenv('SFTP_SERVER_IP'));
define('_SSH_USERNAME_', getenv('SFTP_USERNAME'));

define('_SSH_PORT_', (int)getenv('SSH_PORT') ?: 22);
define('_ENVIRONMENT_', getenv('BITBUCKET_DEPLOYMENT_ENVIRONMENT'));

define('_LOCAL_PROJECT_PATH_', getenv('BITBUCKET_CLONE_DIR'));


if (_ENVIRONMENT_ === 'production') {
    define('_PROJECT_PATH_', getenv('SFTP_PATH_PROD'));
} else {
    define('_PROJECT_PATH_', getenv('SFTP_PATH_DEV'));
}

set('default_stage', _ENVIRONMENT_);

// Use ssh multiplexing to speedup the native ssh client.
set('ssh_type', 'native');

// List of shared files.
set('shared_files', [
    '.htaccess',
//    'wp-config.php',
    'wp-config-smartstore.php',
    'wp-content/db.php',
    'wp-content/object-cache.php',
    'wp-content/wp-cache-config.php',
]);
//
set('shared_dirs', [
    'wp-content/languages',
    'wp-content/uploads',
    'wp-content/w3tc-config'
]);

// List of dirs which must be writable for web server.
set('writable_dirs', [
    'wp-content/cache',
    'wp-content/uploads',
    'wp-content/languages',
    'wp-content/languages/loco',
    'wp-content/languages/loco/plugins',
    'wp-content/languages/loco/themes',
]);

// List of paths which need to be deleted in release after updating code.
set('clear_paths', [
    'wp-content/cache',
]);

set('use_relative_symlink', false);
// Defining a host in Deployer.
host(_SSH_HOST_)
    ->user(_SSH_USERNAME_)
    ->port(_SSH_PORT_)
    ->forwardAgent()
    ->set('deploy_path', _PROJECT_PATH_)
    ->stage(_ENVIRONMENT_)
    ->set('http_user', 'www-data')
    ->multiplexing(true);

/** Tasks */
desc('Execute startup commands');

task('deploy:prepare_code_locally', function () {
    // Pack entire the prepared project to the tar file
    runLocally("tar cvzf /project.tar.gz -X '.deploy-ignore' .");
})
    ->once();

desc('Creating symlink to release');
task('deploy:symlink', function () {
    // Create symlink
    run("cd {{deploy_path}} && {{bin/symlink}} {{release_path}} current");
    // Remove release link.
    run("cd {{deploy_path}} && rm release");
});

desc('Update code');
task('deploy:update_code', function () {
    // Upload the packed project on server

    upload('/project.tar.gz', '{{release_path}}');

    // Unpack the project on the server to release's path
    run('tar -xvzf {{release_path}}/project.tar.gz -C {{release_path}}');

    // Remove the packed file of project
    run('rm {{release_path}}/project.tar.gz');
});

desc('Rewrite flush, clean w3 total cache');
//task('deploy:wp', function () {
//    // Refresh all rewrites
//    run('{{bin/wp}} rewrite flush --path={{release_path}}');
//
//    // Clean W3 total cache
//    run('{{bin/wp}} w3-total-cache flush all --path={{release_path}}');
//})
//    ->once();

task('deploy:completed', function () {
    // Add file to .dep directory, the cron on the server will setup new permissions
    run('touch {{deploy_path}}/.dep/.dep-completed');
})->once();

desc('Reset opcache');
task('deploy:opcache_reset', function () {
    $output = run("{{bin/php}} -r 'opcache_reset();'");
    Writeln('<info>' . $output . '</info>');
});

desc('Reset container');
task('deploy:container_restart', function () {
    if (_ENVIRONMENT_ === 'production') {
        RUN('cd ' . getenv("SFTP_MAIN_PATH_PROD") . ' && docker-compose up -d --force-recreate');
    } else {
        RUN('cd ' . getenv("SFTP_MAIN_PATH_DEV") . ' && docker-compose up -d --force-recreate');
    }


});


task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:prepare_code_locally',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',

    'deploy:symlink',
//    'deploy:wp',
    'deploy:completed',
    'deploy:unlock',
    'cleanup',
    'deploy:opcache_reset',
    'deploy:container_restart'
]);

// If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');